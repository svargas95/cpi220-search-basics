package symbolTable;

/******************************************************************************
 *  Compilation:  javac RedBlackBST.java
 *  Execution:    java RedBlackBST < input.txt
 *  Dependencies: StdIn.java StdOut.java  
 *  Data files:   http://algs4.cs.princeton.edu/33balanced/tinyST.txt  
 *    
 *  A symbol table implemented using a left-leaning red-black BST.
 *  This is the 2-3 version.
 *
 *  Note: commented out assertions because DrJava now enables assertions
 *        by default.
 *
 *  % more tinyST.txt
 *  S E A R C H E X A M P L E
 *  
 *  % java RedBlackBST < tinyST.txt
 *  A 8
 *  C 4
 *  E 12
 *  H 5
 *  L 11
 *  M 9
 *  P 10
 *  R 3
 *  S 0
 *  X 7
 *
 ******************************************************************************/

import java.util.NoSuchElementException;

import util.Queue;
import util.StdIn;
import util.StdOut;

/**
 *  The <tt>BST</tt> class represents an ordered symbol table of generic
 *  key-value pairs.
 *  It supports the usual <em>put</em>, <em>get</em>, <em>contains</em>,
 *  <em>delete</em>, <em>size</em>, and <em>is-empty</em> methods.
 *  It also provides ordered methods for finding the <em>minimum</em>,
 *  <em>maximum</em>, <em>floor</em>, and <em>ceiling</em>.
 *  It also provides a <em>keys</em> method for iterating over all of the keys.
 *  A symbol table implements the <em>associative array</em> abstraction:
 *  when associating a value with a key that is already in the symbol table,
 *  the convention is to replace the old value with the new value.
 *  Unlike {@link java.util.Map}, this class uses the convention that
 *  values cannot be <tt>null</tt>&mdash;setting the
 *  value associated with a key to <tt>null</tt> is equivalent to deleting the key
 *  from the symbol table.
 *  <p>
 *  This implementation uses a left-leaning red-black BST. It requires that
 *  the key type implements the <tt>Comparable</tt> interface and calls the
 *  <tt>compareTo()</tt> and method to compare two keys. It does not call either
 *  <tt>equals()</tt> or <tt>hashCode()</tt>.
 *  The <em>put</em>, <em>contains</em>, <em>remove</em>, <em>minimum</em>,
 *  <em>maximum</em>, <em>ceiling</em>, and <em>floor</em> operations each take
 *  logarithmic time in the worst case, if the tree becomes unbalanced.
 *  The <em>size</em>, and <em>is-empty</em> operations take constant time.
 *  Construction takes constant time.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/33balanced">Section 3.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *  For other implementations, see {@link ST}, {@link BinarySearchST},
 *  {@link SequentialSearchST}, {@link BST},
 *  {@link SeparateChainingHashST}, and {@link LinearProbingHashST},
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 */

public class RedBlackBST<Key extends Comparable<Key>, Value> extends BST<Key, Value>{

    private static final boolean RED   = true;
    private static final boolean BLACK = false;

   // private RBNode root;
    // BST helper RBNode data type
    protected class RBNode extends Node{
        private boolean color;     // color of parent link
        
        public RBNode(Key key, Value val, boolean color, int N) {
            super(key, val, N);
            this.color = color;
          
        }
        
        public RBNode getLeft()
        {
        	return (RBNode)super.getLeft();
        }
        public RBNode getRight()
        {
        	return (RBNode)super.getRight();
        }
        
    }

    /**
     * Initializes an empty symbol table.
     */
    public RedBlackBST() {
    }

   /***************************************************************************
    *  RBNode helper methods.
    ***************************************************************************/
    // is RBNode x red; false if x is null ?
    private boolean isRed(RBNode x) {
        if (x == null) return false;
        return x.color == RED;
    }

 

   /***************************************************************************
    *  Red-black tree insertion.
    ***************************************************************************/

    /**
     * Inserts the key-value pair into the symbol table, overwriting the old value
     * with the new value if the key is already in the symbol table.
     * If the value is <tt>null</tt>, this effectively deletes the key from the symbol table.
     * @param key the key
     * @param val the value
     * @throws NullPointerException if <tt>key</tt> is <tt>null</tt>
     */
    public void put(Key key, Value val) {
    	if (root == null)
    		root = put(null, key, val);
    	else
    		root = put((RBNode)root, key, val);
        ((RBNode)root).color = BLACK;
        // assert check();
    }

    // insert the key-value pair in the subtree rooted at h
    private RBNode put(RBNode h, Key key, Value val) { 
        if (h == null) return new RBNode(key, val, RED, 1);

        int cmp = key.compareTo(h.key);
        if      (cmp < 0) h.left  = put((RBNode)h.getLeft(),  key, val); 
        else if (cmp > 0) h.right = put((RBNode)h.getRight(), key, val); 
        else              h.val   = val;

        // fix-up any right-leaning links
        if (isRed((RBNode)h.getRight()) && !isRed((RBNode)h.getLeft()))      h = rotateLeft(h);
        if (isRed((RBNode)h.getLeft())  &&  isRed((RBNode)h.getLeft().getLeft())) h = rotateRight(h);
        if (isRed((RBNode)h.getLeft())  &&  isRed((RBNode)h.getRight()))     flipColors(h);
        h.N = size(h.getLeft()) + size(h.getRight()) + 1;

        return h;
    }

   /***************************************************************************
    *  Red-black tree deletion.
    ***************************************************************************/

    /**
     * Removes the smallest key and associated value from the symbol table.
     * @throws NoSuchElementException if the symbol table is empty
     */
    public void deleteMin() {
        if (isEmpty()) throw new NoSuchElementException("BST underflow");

        // if both children of root are black, set root to red
        if (!isRed((RBNode)root.getLeft()) && !isRed((RBNode)root.getRight()))
            ((RBNode)root).color = RED;

        root = deleteMin(root);
        if (!isEmpty()) ((RBNode)root).color = BLACK;
        // assert check();
    }

    // delete the key-value pair with the minimum key rooted at h
    private RBNode deleteMin(RBNode h) { 
        if (h.getLeft() == null)
            return null;

        if (!isRed(h.getLeft()) && !isRed(h.getLeft().getLeft()))
            h = moveRedLeft(h);

        h.left = deleteMin(h.getLeft());
        return balance(h);
    }


    /**
     * Removes the largest key and associated value from the symbol table.
     * @throws NoSuchElementException if the symbol table is empty
     */
    public void deleteMax() {
        if (isEmpty()) throw new NoSuchElementException("BST underflow");

        // if both children of root are black, set root to red
        if (!isRed((RBNode)root.getLeft()) && !isRed((RBNode)root.getRight()))
            ((RBNode)root).color = RED;

        root = deleteMax(root);
        if (!isEmpty()) ((RBNode)root).color = BLACK;
        // assert check();
    }

    // delete the key-value pair with the maximum key rooted at h
    private RBNode deleteMax(RBNode h) { 
        if (isRed(h.getLeft()))
            h = rotateRight(h);

        if (h.getRight() == null)
            return null;

        if (!isRed(h.getRight()) && !isRed(h.getRight().getLeft()))
            h = moveRedRight(h);

        h.right = deleteMax(h.getRight());

        return balance(h);
    }

    /**
     * Removes the key and associated value from the symbol table
     * (if the key is in the symbol table).
     * @param key the key
     * @throws NullPointerException if <tt>key</tt> is <tt>null</tt>
     */
    public void delete(Key key) { 
        if (!contains(key)) {
            System.err.println("symbol table does not contain " + key);
            return;
        }

        // if both children of root are black, set root to red
        if (!isRed((RBNode)root.getLeft()) && !isRed((RBNode)root.getRight()))
            ((RBNode)root).color = RED;

        root = delete(root, key);
        if (!isEmpty()) ((RBNode)root).color = BLACK;
        // assert check();
    }

    // delete the key-value pair with the given key rooted at h
    private RBNode delete(RBNode h, Key key) { 
        // assert get(h, key) != null;

        if (key.compareTo(h.key) < 0)  {
            if (!isRed(h.getLeft()) && !isRed(h.getLeft().getLeft()))
                h = moveRedLeft(h);
            h.left = delete(h.getLeft(), key);
        }
        else {
            if (isRed(h.getLeft()))
                h = rotateRight(h);
            if (key.compareTo(h.key) == 0 && (h.getRight() == null))
                return null;
            if (!isRed(h.getRight()) && !isRed(h.getRight().getLeft()))
                h = moveRedRight(h);
            if (key.compareTo(h.key) == 0) {
                Node x = min(h.getRight());
                h.key = x.key;
                h.val = x.val;
                // h.val = get(h.getRight(), min(h.getRight()).key);
                // h.key = min(h.getRight()).key;
                h.right = deleteMin(h.getRight());
            }
            else h.right = delete(h.getRight(), key);
        }
        return balance(h);
    }

   /***************************************************************************
    *  Red-black tree helper functions.
    ***************************************************************************/

    // make a left-leaning link lean to the right
    private RBNode rotateRight(RBNode h) {
        // assert (h != null) && isRed(h.getLeft());
        RBNode x = h.getLeft();
        h.left = x.getRight();
        x.right = h;
        x.color = x.getRight().color;
        x.getRight().color = RED;
        x.N = h.N;
        h.N = size(h.getLeft()) + size(h.getRight()) + 1;
        return x;
    }

    // make a right-leaning link lean to the left
    private RBNode rotateLeft(RBNode h) {
        //assert (h != null) && isRed(h.getRight());
       
        RBNode x = h.getRight(); // wrong, it should be the right
        h.right = x.getLeft(); // wrong, it should be the left
        x.left = h;
        x.color = h.color; // new crash
        h.color = RED;
        x.N = h.N;
        h.N = size(h.getLeft()) + size(h.getRight()) + 1;
        return x;
    }

    // flip the colors of a RBNode and its two children
    private void flipColors(RBNode h) {
        // h must have opposite color of its two children
        // assert (h != null) && (h.getLeft() != null) && (h.getRight() != null);
        // assert (!isRed(h) &&  isRed(h.getLeft()) &&  isRed(h.getRight()))
        //    || (isRed(h)  && !isRed(h.getLeft()) && !isRed(h.getRight()));
        
        h.getLeft().color = !h.getLeft().color;
        h.getRight().color = !h.getRight().color;
    }

    // Assuming that h is red and both h.getLeft() and h.getLeft().getLeft()
    // are black, make h.getLeft() or one of its children red.
    private RBNode moveRedLeft(RBNode h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.getLeft()) && !isRed(h.getLeft().getLeft());

        flipColors(h);
        if (isRed(h.getRight().getLeft())) { 
            h.right = rotateRight(h.getRight());
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }

    // Assuming that h is red and both h.getRight() and h.getRight().getLeft()
    // are black, make h.getRight() or one of its children red.
    private RBNode moveRedRight(RBNode h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.getRight()) && !isRed(h.getRight().getLeft());
        flipColors(h);
        if (isRed(h.getLeft().getLeft())) { 
            h = rotateRight(h);
            flipColors(h);
        }
        return h;
    }

    // restore red-black tree invariant
    private RBNode balance(RBNode h) {
        // assert (h != null);

        if (isRed(h.getRight()))                      h = rotateLeft(h);
        if (isRed(h.getLeft()) && isRed(h.getLeft().getLeft())) h = rotateRight(h);
        if (isRed(h.getLeft()) && isRed(h.getRight()))     flipColors(h);

        h.N = size(h.getLeft()) + size(h.getRight()) + 1;
        return h;
    }
  

   /***************************************************************************
    *  Check integrity of red-black tree data structure.
    ***************************************************************************/
    protected boolean check() {
        if (!isBST())            StdOut.println("Not in symmetric order");
        if (!isSizeConsistent()) StdOut.println("Subtree counts not consistent");
        if (!isRankConsistent()) StdOut.println("Ranks not consistent");
        if (!is23())             StdOut.println("Not a 2-3 tree");
        if (!isBalanced())       StdOut.println("Not balanced");
        return isBST() && isSizeConsistent() && isRankConsistent() && is23() && isBalanced();
    }

    // does this binary tree satisfy symmetric order?
    // Note: this test also ensures that data structure is a binary tree since order is strict
    protected boolean isBST() {
        return isBST(root, null, null);
    }

    // is the tree rooted at x a BST with all keys strictly between min and max
    // (if min or max is null, treat as empty constraint)
    // Credit: Bob Dondero's elegant solution
    protected boolean isBST(RBNode x, Key min, Key max) {
        if (x == null) return true;
        if (min != null && x.key.compareTo(min) <= 0) return false;
        if (max != null && x.key.compareTo(max) >= 0) return false;
        return isBST(x.getLeft(), min, x.key) && isBST(x.getRight(), x.key, max);
    } 

    // are the size fields correct?
    protected boolean isSizeConsistent() { return isSizeConsistent(root); }
    private boolean isSizeConsistent(RBNode x) {
        if (x == null) return true;
        if (x.N != size(x.getLeft()) + size(x.getRight()) + 1) return false;
        return isSizeConsistent(x.getLeft()) && isSizeConsistent(x.getRight());
    } 

    // check that ranks are consistent
    protected boolean isRankConsistent() {
        for (int i = 0; i < size(); i++)
            if (i != rank(select(i))) return false;
        for (Key key : keys())
            if (key.compareTo(select(rank(key))) != 0) return false;
        return true;
    }

    // Does the tree have no red right links, and at most one (left)
    // red links in a row on any path?
    public boolean is23() { return is23((RBNode)root); }
    private boolean is23(RBNode x) {
        if (x == null) return true;
        if (isRed(x.getRight())) return false;
        if (x != root && isRed(x) && isRed(x.getLeft()))
            return false;
        return is23(x.getLeft()) && is23(x.getRight());
    } 

    // do all paths from root to leaf have same number of black edges?
    public boolean isBalanced() { 
        int black = 0;     // number of black links on path from root to min
        RBNode x = (RBNode)root;
        while (x != null) {
            if (!isRed(x)) black++;
            x = x.getLeft();
        }
        return isBalanced((RBNode)root, black);
    }

    // does every path from the root to a leaf have the given number of black links?
    private boolean isBalanced(RBNode x, int black) {
        if (x == null) return black == 0;
        if (!isRed(x)) black--;
        return isBalanced(x.getLeft(), black) && isBalanced(x.getRight(), black);
    } 


    /**
     * Unit tests the <tt>RedBlackBST</tt> data type.
     */
    public static void main(String[] args) { 
        RedBlackBST<String, Integer> st = new RedBlackBST<String, Integer>();
        for (int i = 0; !StdIn.isEmpty(); i++) {
            String key = StdIn.readString();
            st.put(key, i);
        }
        for (String s : st.keys())
            StdOut.println(s + " " + st.get(s));
        StdOut.println();
    }
}
