package clients;

import java.util.ArrayList;
import java.util.List;

import symbolTable.BST;
import symbolTable.I_ST;
import symbolTable.LinearProbingHashST;
import symbolTable.RedBlackBST;
import symbolTable.SeparateChainingHashST;
import symbolTable.SequentialSearchST;
import util.In;
import util.Out;
import util.StdIn;
import util.StdOut;
import util.Stopwatch;

/******************************************************************************

 *  Compilation:  javac FrequencyCounter.java
 *  Execution:    java FrequencyCounter L < input.txt
 *  Dependencies: ST.java StdIn.java StdOut.java
 *  Data files:   http://algs4.cs.princeton.edu/31elementary/tnyTale.txt
 *                http://algs4.cs.princeton.edu/31elementary/tale.txt
 *                http://algs4.cs.princeton.edu/31elementary/leipzig100K.txt
 *                http://algs4.cs.princeton.edu/31elementary/leipzig300K.txt
 *                http://algs4.cs.princeton.edu/31elementary/leipzig1M.txt
 *
 *  Read in a list of words from standard input and print out
 *  the most frequently occurring word that has length greater than
 *  a given threshold.
 *
 *  % java FrequencyCounter 1 < tinyTale.txt
 *  it 10
 *
 *  % java FrequencyCounter 8 < tale.txt
 *  business 122
 *
 *  % java FrequencyCounter 10 < leipzig1M.txt
 *  government 24763
 *
 *
 ******************************************************************************/

/**
 *  The <tt>FrequencyCounter</tt> class provides a client for 
 *  reading in a sequence of words and printing a word (exceeding
 *  a given length) that occurs most frequently. It is useful as
 *  a test client for various symbol table implementations.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/31elementary">Section 3.1</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class FrequencyCounter {

	private static String[] base = new String[] {"Aa", "BB"};

    // Do not instantiate.
    private FrequencyCounter() { }
    
    private static List<String> generateN(int n)
    {
        if(n <= 0)
        {
            return null;
        }

        List<String> list = generateOne(null);
        for(int i = 1; i < n; ++i)
            list = generateOne(list);

        return list;
    }


    private static List<String> generateOne(List<String> strList)
    {   
        if((null == strList) || (0 == strList.size()))
        {
            strList = new ArrayList<String>();
            for(int i = 0; i < base.length; i++)
            {
                strList.add(base[i]);
            }

            return strList;
        }

        List<String> result = new ArrayList<String>();

        for(int i = 0; i < base.length; i++)
        {
            for(String str: strList)
            {   
                result.add(base[i]  + str);
            }
        }

        return result;      
    }
    public static void createHashAttackFile(String filename)
    {
    	
    	Out hashAttackFile = new Out(filename);
    	List list = FrequencyCounter.generateN(18);
    	for (int i = 0; i < list.size(); i++)
    		hashAttackFile.println(list.get(i));
    	System.out.println("N = " + list.size());
    }

    /**
     * Reads in a command-line integer and sequence of words from
     * standard input and prints out a word (whose length exceeds
     * the threshold) that occurs most frequently to standard output.
     * It also prints out the number of words whose length exceeds
     * the threshold and the number of distinct such words.
     */
    public static void main(String[] args) {
        int distinct = 0, words = 0;
        int minlen = Integer.parseInt(args[0]); // av[0] = minlen
       
        I_ST<String, Integer> st = new RedBlackBST<String, Integer>();

       // FrequencyCounter.createHashAttackFile("hashAttack.txt");

        In in = new In("./src/texts/words.txt"); //args[1] is the input file
        Out out = new Out(args[2]); //args[2] is the output file
        System.out.println("Done creating file");        
        Stopwatch sw = new Stopwatch();
        // compute frequency counts
        while (in.hasNextLine()) {
            try
            {
	        	String key = in.readString();
	           
	            
	            if (key != null && key.length() < minlen) continue;
	            //System.out.println(key);
	            words++;
	            
	            
	            // FILL IN
	            // if the symbol table contains the key
	            // increment the value of the symbol table entry
	            if (st.contains(key)) 
	            {
	            	st.put(key,  st.get(key) + 1); // increment after same word twice
	            }
	            else {
	            	st.put(key, 1); // unique new entry
	                
	                distinct++;
	            }
            }
            catch (java.util.NoSuchElementException e)
            {
            	break;
            }
        }
        System.out.println("Elapsed time: " + sw.elapsedTime());
        
        // write the frequency counts to a file
        for (String word : st.keys())
		{
			out.println(word + " " + st.get(word));
		}

        // find a key with the highest frequency count
        String max = "";
        st.put(max, 0);
        for (String word : st.keys()) {
            if (st.get(word) > st.get(max))
                max = word;
        }

        StdOut.println(max + " " + st.get(max));
        StdOut.println("distinct = " + distinct);
        StdOut.println("words    = " + words);
    }
}
